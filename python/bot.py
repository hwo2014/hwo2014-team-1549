import func, util
from math import pi,log

class Bot:
    def __init__(self, name, track, lanes, numLaps):
        self.name = name
        self.track = track
        self.lanes = lanes
        self.curSwitch = 0
        self.numLaps = numLaps
        self.lastPiece = 0
        self.maxDrift = 0
        self.lastDist = 0
        self.lastAngle = 0
        self.turbo = False
        self.turboAvailable = []
        self.lastPosInPiece = 0
        self.curThrottle = 0
        self.pieceIndex = 0
        self.startLane = 0
        self.speed = 0
        self.lastSpeed = 0
        self.remainingStraight = 0
        self.remainingCurve = 0
        self.lastPiece = 0
        self.lastAngle = 0
        self.nextCurve = 0
        self.lastLane = 0
        self.lap = 0
        self.onCurve = False
        self.onCurveLastTick = False

    def update(self, i):
        angle = i['angle']
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        startLane = i['piecePosition']['lane']['startLaneIndex']
        endLane = i['piecePosition']['lane']['endLaneIndex']
        lap = i['piecePosition']['lap']

        self.onCurve = 'angle' in self.track[pieceIndex]
        if self.onCurve:
            self.remainingCurve = self.getRemainingCurve(i)
        else:
            self.remainingStraight = self.getRemainingStraight(i)
        self.nextCurve = self.getNextCurveNum(i)
        self.speed = self.calcSpeed(self.track,i)
        self.onCurveLastTick = self.onCurve
        self.lastAngle = i['angle']
        self.lap = i['piecePosition']['lap']
        self.lastPosInPiece = inPieceDistance
        self.lastPiece2 = self.lastPiece
        self.lastPiece = pieceIndex
        self.lastAngle = angle
        self.lastLane = startLane

        
    def calcSpeed(self, track, i):
        """
        returns current speed of bot
        calculated as the distance covered since the last tick
        TODO:
            fix bug with incorrect speed on switches
        """
        bot = self
        angle = i['angle']
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        startLane = i['piecePosition']['lane']['startLaneIndex']
        speed = 0
        if bot.lastPiece != pieceIndex:
            if u'length' in bot.track[bot.lastPiece]:
                if bot.track[bot.lastPiece]['length'] > bot.lastPosInPiece:
                    speed += bot.track[bot.lastPiece]['length'] - bot.lastPosInPiece
            else:
                if bot.track[bot.lastPiece]['angle'] > 0:
                    if abs(bot.track[bot.lastPiece]['angle'])/360.0*2*pi*(bot.track[bot.lastPiece]['radius'] - bot.lanes[startLane]['distanceFromCenter']) > bot.lastPosInPiece:
                        speed += abs(bot.track[bot.lastPiece]['angle'])/360.0*2*pi*(bot.track[bot.lastPiece]['radius'] - bot.lanes[startLane]['distanceFromCenter']) - bot.lastPosInPiece
                else:
                    if abs(bot.track[bot.lastPiece]['angle'])/360.0*2*pi*(bot.track[bot.lastPiece]['radius'] + bot.lanes[startLane]['distanceFromCenter']) > bot.lastPosInPiece:
                        speed += abs(bot.track[bot.lastPiece]['angle'])/360.0*2*pi*(bot.track[bot.lastPiece]['radius'] + bot.lanes[startLane]['distanceFromCenter']) - bot.lastPosInPiece
            speed += inPieceDistance
        else:
            speed += inPieceDistance - bot.lastPosInPiece
        return speed

    def inFinalStretch(self, i):
        """
        returns True during the final straightaway on the last lap
        """
        bot = self
        lap = i['piecePosition']['lap']
        if lap != self.numLaps -1:
            return False
        pieceIndex = i['piecePosition']['pieceIndex']
        for j in xrange(pieceIndex, len(bot.track)):
            if u'angle' in bot.track[j]:
                return False
        return True

    def onLongestStraight(self, i):
        bot = self
        b = 0
        for j in range(len(bot.track)):
            indr = j
            indl = j-1
            l = 0
            while not u'radius' in bot.track[indr]:
                l += bot.track[indr]['length']
                indr = (indr + 1) % len(bot.track)
            while not u'radius' in bot.track[indl]:
                l += bot.track[indl]['length']
                indl -= 1
            if l > b:
                b = l
        if self.getRemainingStraight(i) > .98 * b:
            return True
        else:
            return False

    def getRemainingCurve(self, i):
        """
        returns distance left in current curve
        """
        bot = self
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        prev = inPieceDistance
        startLane = i['piecePosition']['lane']['startLaneIndex']

        curveLen = 0
        curveAng = bot.track[pieceIndex]['angle']
        curveRad = bot.track[pieceIndex]['radius']
        laneRoff = bot.lanes[startLane]['distanceFromCenter']
        if curveAng > 0:    
            curveLen = 2*pi*(curveRad - laneRoff)
        else:
            curveLen = 2*pi*(curveRad + laneRoff)

        left = (curveAng/360.0)*curveLen - prev
        return left

    def getRemainingStraight(self, i):
        """
        returns distance left in current straightaway
        """
        bot = self
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        prev = inPieceDistance
        left = bot.track[pieceIndex]['length'] - prev
        pieceIndex = (pieceIndex + 1) % len(bot.track)
        while not u'radius' in bot.track[pieceIndex]:
            left += bot.track[pieceIndex]['length']
            pieceIndex = (pieceIndex + 1) % len(bot.track)
        return left

    def getPosInCurve(self, i):
        """
        returns fraction of distance through current curve
        0 <= result <= 1

        TODO:
            figure out how switches affect curve distance
        """
        bot = self
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        prev = inPieceDistance
        startLane = i['piecePosition']['lane']['startLaneIndex']

        curveLen = 0
        curveAng = bot.track[pieceIndex]['angle']
        curveRad = bot.track[pieceIndex]['radius']
        laneRoff = bot.lanes[startLane]['distanceFromCenter']
        if curveAng > 0:    
            curveLen = 2*pi*(curveRad - laneRoff)
        else:
            curveLen = 2*pi*(curveRad + laneRoff)

        left = (curveAng/360.0)*curveLen - prev
        cur = pieceIndex - 1
        while not u'length' in bot.track[cur] and bot.track[cur]['radius'] == curveRad and ((bot.track[cur]['angle'] > 0 and curveAng > 0) or (bot.track[cur]['angle'] < 0 and curveAng < 0)):
            prev += (bot.track[cur]['angle']/360.0)*curveLen
            cur -= 1
        cur = (pieceIndex + 1) % len(bot.track)
        while not u'length' in bot.track[cur] and bot.track[cur]['radius'] == curveRad and ((bot.track[cur]['angle'] > 0 and curveAng > 0) or (bot.track[cur]['angle'] < 0 and curveAng < 0)):
            left += (bot.track[cur]['angle']/360.0)*curveLen
            cur = (cur + 1) % len(bot.track)
        return (prev + 0.0)/(prev + left)

    def getPosInStraight(self, i):
        """
        returns fraction of distance through current straightaway
        0 <= result <= 1

        TODO:
            add extra distance from switches
        """
        bot = self
        pieceIndex = i['piecePosition']['pieceIndex']
        inPieceDistance = i['piecePosition']['inPieceDistance']
        prev = inPieceDistance
        left = bot.track[pieceIndex]['length'] - prev
        cur = pieceIndex - 1
        while not u'angle' in bot.track[cur]:
            prev += bot.track[cur]['length']
            cur -= 1
        cur = (pieceIndex + 1) % len(bot.track)
        while not u'angle' in bot.track[cur]:
            left += bot.track[cur]['length']
            cur = (cur + 1) % len(bot.track)
        return (prev + 0.0)/(prev + left)

    def getNextCurveNum(self, i):
        """
        returns the index of the next curve on the track
        """
        bot = self
        pieceIndex = (i['piecePosition']['pieceIndex'] + 1) % len(bot.track)
        while not u'radius' in bot.track[pieceIndex]:
            pieceIndex = (pieceIndex + 1) % len(bot.track)
        return util.curveNum(bot.track, pieceIndex)
