import math
"""
Models velocity using sigmoid curve:
    y = (k*x)/(1+k-x)
    where 0 <= x,y <= 1

x-axis is stretched based on initial measured data
y-axis is stretched to initial and target speeds
"""

def normalize(data):
    """
    requires data != None, len(data) >= 2
        and at least two different x's and y's
    data is array of pairs
    returns data scaled to [0,1] range
    original data remains unchanged
    """
    ndata = []
    xmax = data[0][0]
    ymax = data[0][1]
    xmin = data[0][0]
    ymin = data[0][1]
    for i in data:
        if i[0] > xmax:
            xmax = i[0]
        if i[1] > ymax:
            ymax = i[1]
        if i[0] < xmin:
            xmin = i[0]
        if i[1] < ymin:
            ymin = i[1]
        ndata.append([i[0],i[1]])
    for i in ndata:
        i[0] -= xmin
        i[1] -= ymin
        i[0] /= (xmax - xmin + .0)
        i[1] /= (ymax - ymin + .0)
    return ndata

def fitness(data, k):
    """
    f(x) = (kx)/(1+k-x)
    requires data to have scale:
        0 <= x <= 1
        0 <= y <= 1
    returns sum of residues
    """
    s = 0
    for i in data:
        x = i[0]
        y = i[1]
        f = k * x / (1 + k - x)
        s += (y - f)**2
    return s

def fitnessFull(data, k):
    """
    f(x) = (kx)/(1+k-x)
    requires data to have scale:
        0 <= x <= 1
        0 <= y <= 1
    returns sum of residues
    """
    s = 0
    for i in data:
        x = i[0] * 2 - 1
        y = i[1] * 2 - 1
        f = k * x / (1 + k - abs(x))
        s += (y - f)**2
    return s

def ternarySearch(leftBound, rightBound, precision, data, fitFunc):
    """
    uses ternary search to find minimum of fitness function,
        within precision value
    requires minimum to be within right and left bounds
    """
    ndata = normalize(data)
    while True:
        if abs(rightBound - leftBound) < precision:
            return (leftBound + rightBound)/2
        leftThird = (2*leftBound + rightBound)/3
        rightThird = (leftBound + 2*rightBound)/3
        if fitFunc(ndata,leftThird) > fitFunc(ndata,rightThird):
            leftBound = leftThird
        else:
            rightBound = rightThird

def xRange(data):
    """
    requires data to have at least two different x values,
    returns width of range of x values
    """
    xmin = data[0][0]
    xmax = data[0][0]
    for i in data:
        if i[0] < xmin:
            xmin = i[0]
        if i[0] > xmax:
            xmax = i[0]
    return xmax - xmin

def sigFit(data, precision = 0.0000000000001):
    """
    returns sigmoid function constant k
        -1.0000000000001 >= k >= -100
        and the scale for x
    """
    return ternarySearch(-1.0000000000001,-100,precision,data,fitness), xRange(data)

def sigFitFull(data, precision = 0.0000000000001):
    """
    returns sigmoid function constant k
        -1.0000000000001 >= k >= -100
        and the scale for x
        for curve f(x) = kx/(1 + k - abs(x))
    """
    return ternarySearch(-1.0000000000001,-100,precision,data,fitnessFull), xRange(data)

def timeToBrake(initial, final, k, m, h=0):
    if k != 0 and (initial - (h/k)) != 0:
        return math.log((final - (h/k))/(initial - (h/k)))*m / (-1*k)
    return 4

def speedAfterTicks(initial, t, h, k, m):
    v = (initial - (h/k))*math.e**((-1*k*t)/m) + (h/k)
    return v

def distanceAfterTicks(initial, t, h, k, m):
    d = (m/k)*(initial - (h/k))*(1.0-math.e**((-1*k*t)/m) + (h/k)*t)
    return d

def getThrottle(v1, v2, k, m):
    h = k*(v2*math.e**((-1*k/t)/m) - v1)/(math.e**((k*t)/m) - 1)
    return h

def linSolver(d1,d2):
    """
    input: 2 arrays of data points in form:
        alpha, omega, theta

    returns a and b from equation:
        alpha + a*omega + b*theta = 0

    alpha1 + a*omega1 + b*theta1 = 0
    a = (-alpha1 - theta1*b)/omega1
    alpha2 + a*omega2 + b*theta2 = 0

    alpha2 + (-alpha1 - theta1*b)/omega1 *omega2 + b*theta2 = 0
    =>  -alpha1*omega2/omega1 - theta1*omega2*b/omega1 + b*theta2 = -alpha2
    =>  b(theta2 - theta1*omega2/omega1) = alpha1*omega2/omega1 - alpha2
    =>  b = (alpha1*omega2/omega1 - alpha2)/(theta2 - theta1*omega2/omega1)

    =>  a = (-alpha1 - theta1*b)/omega1
    """
    alpha1 = d1[0]
    omega1 = d1[1]
    theta1 = d1[2]

    alpha2 = d2[0]
    omega2 = d2[1]
    theta2 = d2[2]

    b = (alpha1*omega2/omega1 - alpha2)/(omega2 - theta1*omega2/omega1)
    a = (-1*alpha1 - theta1*b)/omega1
    return a,b

