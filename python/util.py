"""
Helper functions for pathfinding and calculating track constants
"""
from math import pi

def getGraph(track, lanes, laps = 0):
    """
    calculates distance to finish line along optimal path
        for every point on track

    returns list of path lengths for each lane for each switch
    e.g. [300,450,400]:
    switching left will give optimal path length of 300
    """
    numl = len(lanes)
    graph = [[[0,0,0] for i in range(numl)]]
    for i in range(len(track)):
        if u'switch' in track[i]:
            graph.append([[0,0,0] for j in range(numl)])
    ind = len(graph) - 2
    for i in range(len(track) - 1, -1, -1):
        if u'switch' in track[i]:
            #initialize edge weights for next switch
            ind -= 1
            for j in range(len(graph[ind])):
                if j == 0:
                #left lane (can't switch left)
                    graph[ind][j][0] = 99999999999
                    graph[ind][j][1] = min(graph[ind+1][j])
                    graph[ind][j][2] = min(graph[ind+1][j+1])
                elif j == len(graph[ind]) - 1:
                #right lane (can't switch right)
                    graph[ind][j][0] = min(graph[ind+1][j-1])
                    graph[ind][j][1] = min(graph[ind+1][j])
                    graph[ind][j][2] = 99999999999
                else:
                    graph[ind][j][0] = min(graph[ind+1][j-1])
                    graph[ind][j][1] = min(graph[ind+1][j])
                    graph[ind][j][2] = min(graph[ind+1][j+1])
        if u'length' in track[i]:
            #straight piece
            for j in range(len(graph[ind])):
                graph[ind][j][0] += track[i]['length']
                graph[ind][j][1] += track[i]['length']
                graph[ind][j][2] += track[i]['length']
        elif u'radius' in track[i]:
            #turn piece
            arcs = []
            if track[i]['angle'] > 0:
                #turning right
                arcs = [abs(track[i]['angle'])/360.0*2*pi*(track[i]['radius'] - j['distanceFromCenter']) for j in lanes]
            else:
                #turning left
                arcs = [abs(track[i]['angle'])/360.0*2*pi*(track[i]['radius'] + j['distanceFromCenter']) for j in lanes]
            for j in range(len(graph[ind])):
                if j == 0:
                #left lane (can't switch left)
                    graph[ind][j][1] += arcs[0]
                    graph[ind][j][2] += arcs[1]
                elif j == len(graph[ind]) - 1:
                #right lane (can't switch right)
                    graph[ind][j][0] += arcs[-2]
                    graph[ind][j][1] += arcs[-1]
                else:
                    graph[ind][j][0] += arcs[j-1]
                    graph[ind][j][1] += arcs[j]
                    graph[ind][j][2] += arcs[j+1]
    #casting everything int for easier logging
    #output will still be optimal
    for i in graph:
        for j in i:
            j[0] = int(j[0])
            j[1] = int(j[1])
            j[2] = int(j[2])
    graph.pop()
    return graph

def getNextSwitch(graph, lane, switch):
    """
    Given a graph, current lane, and switch number
    returns decision that gives optimal path
    """
    if graph[switch][lane][0] == graph[switch][lane][1]:
        return None
    if graph[switch][lane][1] == graph[switch][lane][2]:
        return None
    if lane == 0:
        #left lane (can't switch left)
        if graph[switch][0][1] < graph[switch][0][2]:
            return None
        else:
            return "Right"
    if lane == len(graph[switch])-1:
        #right lane (can't switch right)
        if graph[switch][-1][1] < graph[switch][-1][0]:
            return None
        else:
            return "Left"
    ind = graph[switch][lane].index(min(graph[switch][lane]))
    return ["Left", None, "Right"][ind]

def curveNum(track, pieceIndex):
    """
    returns an integer corresponding to which curve you are on.
    for straightaways, returns last curve.
    turn pieces are in the same curve if they are adjacent
        and have the same radius and turn direction
    """
    curve = -1
    prevRad = 0
    prevAng = 1
    for j in range(pieceIndex + 1):
        if j >= len(track):
            j = 0
        if u'angle' in track[j]:
            if track[j]['angle']/abs(track[j]['angle']) != prevAng:
                curve += 1
                prevRad = track[j]['radius']
                prevAng = track[j]['angle']/abs(track[j]['angle'])
            elif track[j]['radius'] != prevRad:
                curve += 1
                prevRad = track[j]['radius']
                prevAng = track[j]['angle']/abs(track[j]['angle'])
    return curve

def numCurves(track):
    l = 0
    for i in range(len(track)-1,-1,-1):
        if curveNum(track,i) != l:
            return curveNum(track,i)

def reduceMaxSpeeds(track, lanes, C):
    """
    returns list of maximum speeds for each curve on the track
    uses physics equation:
        v < sqrt(T*r)
    """
    prevCurveNum = -1
    res = []
    for i in range(len(track)):
        if u'angle' in track[i]:
            curve = curveNum(track, i)
            if curve != prevCurveNum:
                rad = track[i]['radius']
                res.append([(C*(rad+i['distanceFromCenter']))**.5 for i in lanes])
                prevCurveNum = curve
    print res
    return res


