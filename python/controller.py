import util
import func
from math import pi,log


def decide(chillbot, i):
    """
    main decision code ran during each tick
    """
    bot = chillbot.bot
    angle = i['angle']
    pieceIndex = i['piecePosition']['pieceIndex']
    inPieceDistance = i['piecePosition']['inPieceDistance']
    startLane = i['piecePosition']['lane']['startLaneIndex']
    endLane = i['piecePosition']['lane']['endLaneIndex']
    lap = i['piecePosition']['lap']

    #very first tick
    if chillbot.MAXSPEEDS == None:
        chillbot.MAXSPEEDS = util.reduceMaxSpeeds(bot.track, bot.lanes, chillbot.C)
        bot.lastPosInPiece = inPieceDistance
        bot.lastPiece = pieceIndex
        bot.lastAngle = angle
        bot.lastLane = startLane

    if chillbot.K == None:
        lap0(chillbot, i)
    else:
        if u'switch' in bot.track[(pieceIndex + 1) % len(bot.track)] and pieceIndex != bot.lastPiece2:
            queueSwitch(chillbot, i)
        #targThrottle = .92
        targThrottle = .654
        #targThrottle = .51
        targThrottle = chillbot.topCurveSpeeds[bot.getNextCurveNum(i)]/chillbot.maxV
        bot.curThrottle = targThrottle
        if bot.inFinalStretch(i):
            if not bot.turbo and bot.turboAvailable != []:
                chillbot.goTurbo()
            bot.curThrottle = 1
        elif u'length' in bot.track[pieceIndex]:
            if not bot.turbo and bot.turboAvailable != [] and bot.onLongestStraight(i):
                chillbot.goTurbo()
            if bot.speed != 0 and bot.remainingStraight/(0.0+bot.speed) > func.timeToBrake(bot.speed, 10*targThrottle, chillbot.K, chillbot.M):
                bot.curThrottle = 1
            else:
                bot.curThrottle = targThrottle
        targetSpeed(bot)

    if abs(angle) > bot.maxDrift:
        chillbot.maxDrift = abs(angle)
        if u'angle' in bot.track[pieceIndex]:
            r = 0
            if bot.track[pieceIndex]['angle'] > 0:
                r = bot.track[pieceIndex]['radius'] - bot.lanes[startLane]['distanceFromCenter']
            else:
                r = bot.track[pieceIndex]['radius'] + bot.lanes[startLane]['distanceFromCenter']
            chillbot.C = bot.speed**2/r
            chillbot.cSpeed = bot.speed

    chillbot.throttle(bot.curThrottle)
    if bot.turbo:
        if bot.turboLeft > 0:
            bot.turboLeft -= 1
        if bot.turboLeft == 0:
            bot.turbo = False

def targetSpeed(bot):
    """
    uses max and min throttle to get to target speed
    """
    if not bot.turbo:
        if bot.speed < 10*bot.curThrottle - .05:
            bot.curThrottle = 1
        elif bot.speed > 10 * bot.curThrottle:
            bot.curThrottle = 0
    else:
        if bot.curThrottle != 1:
            if bot.speed < 10*bot.curThrottle -.05:
                bot.curThrottle = 1
            elif bot.speed > 10 * bot.curThrottle:
                bot.curThrottle = 0

def queueSwitch(chillbot, i):
    """
    gets next switch decision, and queues it
    increments bot.curSwitch
    """
    bot = chillbot.bot
    pieceIndex = i['piecePosition']['pieceIndex']
    startLane = i['piecePosition']['lane']['startLaneIndex']
    s = util.getNextSwitch(chillbot.graph, startLane, bot.curSwitch)
    if s != None:
        chillbot.switch(s)
    elif chillbot.needToSwitchLanes:
        if startLane == 0:
            chillbot.switch("Right")
        else:
            chillbot.switch("Left")
        chillbot.needToSwitchLanes = False
    bot.curSwitch = (bot.curSwitch + 1) % (len(chillbot.graph))

def lap0(chillbot, i):
    """
    First lap of qualifying round:
    gather data, compute
        velocity curves
        max speed around curves
    TODO:
        find way to calculate max speed per curve
    """
    bot = chillbot.bot
    pieceIndex = i['piecePosition']['pieceIndex']
    angle = i['angle']
    if chillbot.K == None:
        h = .3
        bot.curThrottle = h
        if len(chillbot.kmdata) == 3:
            v1 = chillbot.kmdata[0]
            v2 = chillbot.kmdata[1]
            v3 = chillbot.kmdata[2]
            K = (v1-(v2-v1))/(v1**2)*h
            M = 1.0/(log((v3-(h/K))/(v2-(h/K)))/(-1*K))
            chillbot.K = K
            chillbot.M = M
            chillbot.maxV = 1.0/K
        else:
            if bot.speed != 0:
                chillbot.kmdata.append(bot.speed)
    else:
        if u'switch' in bot.track[(pieceIndex + 1) % len(bot.track)] and pieceIndex != bot.lastPiece:
            queueSwitch(bot, i)
        targThrottle = .6
        bot.curThrottle = targThrottle
        #bot.curThrottle = .55
        #bot.curThrottle = .51
