import json
import socket
import sys
from math import pi
import util
import controller
from bot import *

LOGGING = False

class ChillBot(object):

    MAXANGLE = 60.0
    MAXSPEEDS = None
    MAGIC = .50625
    sigData = [[0.0,0.0]]
    SIG = None
    K = None
    M = None
    maxV = 1
    kmdata = []
    C = 0.455239999999
    cSpeed = 0
    topCurveSpeeds = []
    cars = {}
    bot = None
    crashes = []

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.maxDrift = 0
        self.turboFactor = 1
        self.turboLeft = 0
        self.lastSwitchQueued = -1
        self.needToSwitchLanes = False
        self.lastPosInPiece = 0
        self.data = ["speed,throttle,drift,angle,radius,angleInTurn"]
        print self.name

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def switch(self, direction):
        self.msg("switchLane", direction)

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        #return self.msg("joinRace", {"name": self.name, "key": self.key})
        return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def goTurbo(self):
        if self.bot.turboAvailable != []:
            print "turbo activated"
            self.bot.turboAvailable.pop()
            self.bot.turbo = True
            self.msg("turbo", "Pow pow pow pow pow, or your of personalized turbo message")

    def on_turbo_available(self, data):
        print "turbo available"
        self.bot.turboAvailable.append(0)
        self.bot.turboLeft = data['turboDurationTicks']
        self.bot.turboFactor = data['turboFactor']
    
    def on_game_init(self, data):
        self.lastSwitchQueued = -1
        self.track = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']
        self.carsData = data['race']['cars']
        self.startingPoint = data['race']['track']['startingPoint']
        if 'laps' in data['race']['raceSession']:
            self.numLaps = data['race']['raceSession']['laps']
        else:
            self.numLaps = 0
        self.graph = util.getGraph(self.track, self.lanes)
        for i in self.carsData:
            self.cars[i['id']['name']] = Bot(i['id']['name'], self.track, self.lanes, self.numLaps)
            if i['id']['name'] == self.name:
                self.bot = self.cars[i['id']['name']]
                self.topCurveSpeeds = [1.0]*util.numCurves(data['race']['track']['pieces'])
                self.carLength = i['dimensions']['length']
                self.carWidth = i['dimensions']['width']
                self.guideFlagPosition = i['dimensions']['guideFlagPosition']

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def decide(self, data):
        controller.decide(self, data)
        s = ""
        if u'angle' in self.bot.track[self.bot.lastPiece]:
            s = str(self.bot.track[self.bot.lastPiece]['angle']) +","+ str(self.bot.track[self.bot.lastPiece]['radius'] - self.bot.lanes[self.bot.lastLane]['distanceFromCenter'])
            s += "," + str(self.bot.getPosInCurve(data))
        else:
            s = "0,0,0"
        self.data.append(str(10*self.bot.speed) +
                         "," + str(100*self.bot.curThrottle) +
                         "," + str(self.bot.lastAngle) +
                         "," + s)

    def getCarPositions(self,data):
        sortedByLap = sorted(data, key = lambda x:x['piecePosition']["lap"])
        sortedByPiece = sorted(sortedByLap, key = lambda x:x["piecePosition"]["pieceIndex"])
        return sorted(sortedByPiece, key = lambda x:x["piecePosition"]["inPieceDistance"])

    def carsInLaneByPosition(self,data):
        return filter(lambda x:x["piecePosition"]['lane']["startLaneIndex"]==self.startLane,self.getCarPositions(data))
    
    def carsAheadInSameLane(self,data):
        index=0
        cdata = self.carsInLaneByPosition(data)
        for car in cdata:
            if car["id"]["name"] == self.name:
                break
            index+=1
        return [i['id']['name'] for i in cdata[index+1:]]

    def on_car_positions(self, data):
        for i in data:
            name = i['id']['name']
            self.cars[name].update(i)
            if name == self.name:
                self.decide(i)
            else:
                if self.cars[name].lastPiece >= self.bot.lastPiece and self.cars[name].lastPiece < self.bot.lastPiece + 4:
                    if self.cars[name].speed <= self.bot.speed:
                        self.needToSwitchLanes = True
        for j in self.cars:
            currentBot = self.cars.get(j)
            if currentBot.onCurveLastTick and not currentBot.onCurve:
                if topCurveSpeeds[util.curveNum(currentBot.track,currentBot.pieceIndex)] < currentBot.lastSpeed or topCurveSpeeds[util.curveNum(currentBot.track,currentBot.pieceIndex)] == None:
                    topCurveSpeeds[util.curveNum(currentBot.track,currentBot.pieceIndex)] = currentBot.lastSpeed


    def on_crash(self, data):
        print("Someone crashed")
        self.crashes.append(data['name'])
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print "maxDrift: " + str(self.maxDrift)
        print bot.C, bot.cSpeed
        print self.topCurveSpeeds
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'gameEnd':
                    self.gameId = msg['gameId']
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = ChillBot(s, name, key)
        #bot.trackName = "usa"
        bot.trackName = "keimola"
        #bot.trackName = "germany"
        bot.numCars = 1
        bot.run()
        if LOGGING:
            f = open("logs/" +  bot.trackName + "/" + name + "_"+str(bot.gameId)+ ".csv", 'w')
            for i in bot.data:
                f.write(str(i) + "\n")
            f.close()
            f = open("logs/races.txt", "a")
            link = "https://helloworldopen.com/race-visualizer/?recording=https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/"
            f.write(bot.trackName + "::" +name+"::"+link+str(bot.gameId)+".json\n")
            f.close()

